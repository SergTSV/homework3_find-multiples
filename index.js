// part 1

let num = +prompt('Enter integers number', '');

while (!Number.isInteger(num)) {
  num = +prompt("Input error, use integers for input", '');
}
if (num < 0) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 0; i <= num; i++) {
         if (i % 5 !== 0) continue;
        console.log(i);
    }
}

// part 2

let m = 0;
let n = 0;

while (m >= n) {
    m = +prompt('Enter a smaller integer => 2', '');
    while (!Number.isInteger(m) || (m < 2)) {
        m = +prompt("Input error, use integers for input => 2", '');
    }
    n = +prompt('Enter a larger integer  > first number', '');
    while (!Number.isInteger(n) || (n < m)) {
        n = +prompt("Input error, use integers for input  > first number", '');
    }
}
    loop:
        for (let i = 2; i <= n; i++) {
            if (i < m) continue;
            for (let j = 2; j < i; j++) {
                if (i % j === 0) continue loop;
            }
            console.log(i)
        }

